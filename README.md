# Seguretat en les comunicacions digitals

Nocions bàsiques de seguretat en l’ús d’eines de comunicació digitals

## Antecedents
* Des de les revelacions d’Edward Snowden (2014)
    * Ja ningú no pot negar l'espionatge massiu ni la col·laboració d’operadors d'infraestructures i empreses de serveis
* Els grans aliats per a l'espionatge:
    * Manca de “cultura i consciència digital”
    * Traça digital
    * Patrons de comportament
* “Jo no tinc res a amagar”
    * Segur? I en el futur? En qualsevol cas, val més prevenir ;-)
    * Informació que ens pot semblar irrellevant en el moment de generar-la o propagar-la pot ser utilitzada més endavant en contra nostra (i amb conseqüències molt significatives)

## Fonts informació
* Informació que nosaltres mateixos facilitem
    * Conscientment
        * Xarxes socials, xats, e-correus, arxius copiats, etc.
    * Inconscientment
        * Metadades, goeposicionament, pagaments amb tarja, càmeres de trànsit, etc.
* Informació que ens “roben”
    * Intercepció de comunicacions
        * Internet és insegur per disseny (per molt que ens diguin “ara esteu connectats a una xarxa segura”). La seguretat s’ha de buscar a nivell d’aplicació (navegadors, programari de missatgeria instantània, etc.)
    * Robatori/pèrdua d’aparells
        * Telèfon mòbil, tualetes, portàtil, llapis USB, etc.
    * Infiltració de programari no desitjat
        * Escoltes, còpia d’informació, etc.

##Objectius
1. Que no puguin prova que una informació és nostre
2. (si 1. no és possible) Dificultar/retardar l’accés a la informació (impedir-lo totalment és, en la majoria dels casos, impossible)

## Estratègia
* Hem de saber trobar el compromís entre seguretat i comoditat
    * Què volem assegurar (a què ens exposem si perdem aquesta informació) vs. què ens costa assegurar-ho
        * _Exemple:_ és molt còmode que els missatges de xat surtin per pantalla automàticament fins i tot quan el dispositiu està aturat, però si el l’aparell cau en males mans ells tb podran llegir els missatges que van arribant, encara que no el puguin desbloquejar 
* Depenent de com de comprometedora sigui la informació que managuem haurem de
    1. Millorar la seguretat en l'ús d'aquelles eines i hàbits als quals estem acostumats
    2. Adoptar nous hàbits i noves eines si no estem segurs que podem assegurar les que fem servir habitualment
* Conducta
    * és bo integrar aquests hàbits en la vida normal, no només quan vulguem amagar quelcom, pq llavors ja aixequem sospites
        * _Exemple:_ molt millor Telegram que Whatsapp i Signal que Telegram i encara millor si ho combinem

## Solucions - Conceptes generals
* Per sort, amb unes poques mesures addicionals augmentem molt la seguretat
    * Per segures que siguin les eines sempre hem d’estar al cas a fer-ne un bon ús i sempre identificar quin és el punt més dèbil en la cadena de tractament de la informació i millorar-ne la seguretat
        * _Exemple:_ podem tenir el mòbil molt ben protegit (tot xifrat, tot amb passwds supersegurs, etc.), però si a l’ordinador també hi tenim la sessió de Telegram i no tenim l’ordinador protegit, només accedint a l’ordinador es pot accedir directament a totes les converses no privades
* Eines bàsiques:
    * Criptografia de clau pública / clau privada:
        * Ens facilita el xifratge extrem-a-extrem (emissor-receptor)
        * Ben usat es considera molt segur
    * Programari lliure
        * És auditable: podem assegurar (els experts ho fan per nosaltres) que el programari fa allò que ens diuen que fa i no fa res més (com per exemple enviar còpies a un altre lloc)
        * _Exemples:_
            * Firefox millor que Internet Explorer
            * Libreoffice millor que Microsoft Office
            * Qualsevol distribució de Linux (Ubuntu, Debian, Redhat, etc.) millor que Windows o Mac
                * _Nota:_ Mac té certa reputació de ser un sistema molt segur, però des del punt de vista d'anàlisi del programari no podem dir massa cosa perquè la major part és privatiu i, per tant, no es pot auditar
                * _Recomanació:_ Tails és una distribució "live" de Linux molt segura
* Seguretat a nivell d’aplicació vs. seguretat a nivell de xarxa
    * Són compatibles
    * Seguretat a nivell d’aplicació
        * Cal acostumar-se a fer servir aplicacions segures (xifrades)
            * _Exemple:_ Signal millor que el correu tradicional (sense xifratge) perquè xifra totes les converses (també les de grup _Atenció:_ ni Telegram ni Whatsapp ho fan)
    * Seguretat a nivell de xarxa
        * Per assegurar-se que no deixem traces als nodes intermitjos d’internet (per exemple als routers dels ISPs)
        * _Recomanació:_ Bàsicament a través de xarxa Tor

## Recomanacions bàsiques
* No instal·lem programari ni fem click a enllaços sense un mínim de seguretat
  * A banda de virus, ara hi ha els programes d'espionatge
  * Que tinguem un antivirus instal·lat i que no ens avisi d’una amenaça no vol pas dir que l’amenaça no sigui real
* Només activem i utilitzem aquelles funcionalitats que necessitem només quan les necessitem
* Connecitivitat
  * Deshabilitem sempre Bluetooth, NFC i la compartició de connexió
* Geoposicionament
    * No el tinguem activat si no és el necessitem
        * _Exemple:_ només activem-lo quan necessitem saber la nostra posició sobre el mapa i apaguem-lo immediatament després (així, per exemple, ens assegurem que la càmera de fotos no registra la posició a les fotos que fem)
    * Aturem els mòbils (traient bateria si cal) o deixem-los a d'altres llocs (millor, perquè no aixequem la sospita a través de la desconnexió)
* Trucades de veu
    * Hem de considerar que les trucades estàndard són sempre espiades
    * Fem servir sempre que puguem sistemes alternatius (Signal, jit.si, etc.)
* Escoltes
    * Hi ha casos provats de programari maliciós infiltrat que permet l’accés als micros i a les càmeres
    * Deixem els mòbils a una altra sala quan ens reunim
        * També ens ajudarà a centrar l’atenció en la reunió :-)

## Mòbil / tauletes / portàtil / PC
* Sempre actiu el bloqueig
    * És molt més segur l’ús de codi que no pas el de patró
        * Codi: més difícil d’endevinar; patró: deixa marques a la pantalla
    * Deixeu els dispositius sempre bloquejats. Tingueu el botó de bloqueig sempre apunt.
* Notificacions deshabilitatdes a la pantalla de bloqueig
    * Perquè no puguin llegir el que anem rebent si cauen en males mans
    * Es poden deshabitar a nivell de cada aplicació o a nivell de tot el dispositiu
* Si es pot: 1) xifrat de sistema i 2) xifrat de dades (tots dos!)
    * No tots els dispositius o models ho suporten (assegureu-vos que el proper que compreu ho suporta)
    * L’activació pot comportar la pèrdua de totes les dades pre-existents
        * Molt millor fer-ho quan estrenem els dispositius
        * Fem còpies de seguretat (controlades) abans de xifrar
* Actualitzacions
    * Només aquelles que siguin de seguretat
* Molt atenció a les còpies descontrolades d’informació:
    * Les que fem nosaltres: correus electrònics, dades al núvol, pendrives, etc.
    * Les que fan els sistemes: còpies de seguretat, memòries cau (caches), etc.

## Android
* F-Droid (repositori d'aplicacions)
    * És una alternativa a Google Play per instal·lar programari (lliure)
    * Tot el programari és lliure i està molt auditat (ens en podem refiar)
    * Instal·lació:
        1. Momentàniament hem de permetre al sistema instal·lar paquets d’orígens desconeguts. Ho haurem de fer cada vegada que instal·lem un paquet d’F-Droid. __SEMPRE IMMEDIATAMENT DESPRÉS deshabilitarem els orígens desconeguts__
            1. seguretat → origens desconneguts (activar/__DESACTIVAR__)
        1. Navegador a https://f-droid.org/ → ”Descàrregar F-Droid”
        1. Descarregues (desplegable superior) → clic sobre Fdorid.apk
        1. Executar F-Droid
            1. Opcions → Gestiona els dipòsits → Habilitar “Guardian Project Official Releases”
            1. Actualitzar dipòsits (botó dalt-dreta, cercle amb triangle) i esperar que apareguin continguts
    * __SEMPRE ENS ASSEGURAREM QUE DEIXEM desactivat "origens desconeguts"__
        1. seguretat → origens desconneguts (DESACTIVAT)
* Lineage (sistema operatiu)
    * És una alternativa a l’Android que ens ve de fàbrica (no és trivial d’instal·lar i podem bloquejar el mòbil)
    * Ens permet assegurar que no tenim programari maliciós que pogués venir de fàbrica
    * També és un sistema Android

## Apps de xat i veu
* De més a menys segures
    * Riot – molt segur, però una mica incòmode de fer servir
    * Signal – bon compromís seguretat i usabilitat; recomanat per ús estàndard; moltes opcions de seguretat
    * Telegram – es considera segur; força popular; els “canals” permeten arribar a gran quantitat de gent (canals dels CDR, Assamblea, Òmnium, EnPeuDePau, etc.)
    * Whatsapp – no es considera segur; no recomanat
* FEM-LES SERVIR TAMBÉ PER A TRUCADES DE VEU SEMPRE QUE PUGUEM (cobertura de dades)
* Missatges efímers
    * Programem un temps per a destrucció de missatge per no deixar missatges descontrolats (_Exemple:_ una setmana)
    * Signal, Telegram
* Xifratge punt a punt
    * Riot – per defecte
    * Signal – per defecte grups inclosos
    * Telegram – només si s'activa l'opció “xat secret” i només entre dues persones, no en grup
    * Whatsapp - per defecte només entre dues persones, no en grup
* Problema: totes les apps anteriors depenen de la connexió a internet. Sense connexió a internet:
    * Mesh fent servir tecnologies disponibles al mòbil (WiFi, Bluetooth)
        * Serval Project
        * Firechat – mesh
    * Construïnt i operant nosaltres mateixos la xarxa (sobirania tecnològica)
        * guifi.net

## Comunicació PC
* Xat
    * https://web.whatsapp.com (per web), https://web.telegram.org (per web), https://www.signal.org/download/ (cal baixar i instal·lar l'aplicació)
* Video-conferència
  * jit.si – alternativa a skype (skype és molt insegur!); també per a mòbil amb app
  * https://meet.jit.si/ElqueVolgueu
    * Tothom que vagi a l'adreça https://meet.jit.si/ElqueVolgueu entrarà al canal ElqueVulgueu
    * Si hi ha algú que no coneixeu, sortiu del canal i através d’algun altre mètode (p.ex. Telegram) acordeu-ne un altre canal
* Edició col·laborativa
   * No fem servir eines al núvol (_Exemple:_ No fem servir googledocs)
   * _Recomanació:_
        * http://piratepad.net/ElqueVulgueu
        * Tothom que vagi a https://piratepad.net/ElqueVulgueu podrà editar text pla
        * Quan acabeu, esborreu-ho tot

## Navegadors WEB
* Bones pràctiques (sobretot en dispositius que no són vostres)
    * Navegar en mode incògnit
        * Quan tanqueu la sessió s’esborren automàticament l’historial i les galetes (cookies) però no el que hagueu baixat (documents, etc.) 
    * Esborrar historial quan acabem
    * Assegurar-se que sempre fem servir comunicacions xifrades, és a dir que l’adreça comença per https (no http) i/o apareix un candau o icona similar
    * Buscador recomanat: https://duckduckgo.com (no deixa traces; google.com en deixa moltes)
* Tor
    * Abans de començar a navegar comproveu que efectivament esteu connectats correctament
        * https://check.torproject.org
    * Mòbil
        * Orbot + Orfox (Orbot proveix l'accés a la xarxa Tor; Orfox és un navegador segur)
        * disponible a F-Droid al repositori “Guardian Project Official Releases”
        * Engegant Orfox ja s'engega Orbot
    * PC
        * Torbrowser-launcher
            * Disponible per a Windows, Linux i Mac (https://www.torproject.org/download/download)

## Tor
(A banda de la navegació WEB ja comentada a la secció anterior)
* Proporciona una xarxa virtual anònima a sobre d’Internet
* Mòbils
    * Orbot és l’aplicació que es connecta a la xarxa Tor
    * A l’inici d’Orbot o durant la seva execució podem associar-hi aplicacions pq vagin per la xarxa Tor (utilitza el connector VPN):
        * Per fer-ho clickar sobre el botó “APPS...”
        * _Exemple:_ Singal sobre Orbot
    * Hi ha aplicacions que permeten utilitzar el proxy local d’Orbot:
        * Orfox ho fa automàticament
        * Telegram → Ajustos → Dades i emmagatzematge → Ajustos de Proxy → IP 127.0.0.1 port 9050
* PC/Punt d’Accés
    * Tor + proxy socks (no trivial)
* Tant per PC com per mòbil, també es pot fer per túnel xifrat fins a l'extranger i llavors entrar a la xarxa Tor

## Màquines virutals
* Són "un ordinador (sistema operatiu) dins del nostre ordinador (sistema operatiu)"
* En podem anar creant i destruïnt a conveniència (per exemple, per cada cosa comprometedora)
* _exemple:_ VirtualBox

## Altres traces
* Metadades
    * Són dades amagades dins dels arxius
        * _Exemple:_ geolocalització a les fotos, autor als documents de text
    * Abans de enviar arxius, assegureu-vos que no hi ha dades personals
        * _Exemple:_ Libreoffice Fitxer → Propietats ...
    * _Recomanació:_ Prioritzeu l'ús d'eines i formats de documents que tingueu controlats
        * _Exemple:_ Millor un Copiar&Enganxar en text pla que no pas un fitxer d'un programa que genera uns documents superxulos, però que no sabem què guarda ni com ho guarda ni a on
* Pagaments
    * Metàl·lic millor que tarja
    * Les Monedes digitals (BitCoin, LiteCoin, FairCoin, etc.)
* Quan publiquem de forma anònima:
    * Assegurem que no queden metadades que ens comprometin
    * Navegueu sobre Tor
    * Feu servir comptes anònims
        * Per obrir els comptes podeu emprar adreces de correu volàtils
            * _Exemple:_ https://www.guerrillamail.com/
        * i feu-ho en diversos salts (compte 1 per obrir compte 2, etc.)
* Buscadors internet
    * Els buscadors comercials (Google, Yahoo, etc.) guarden les dades de totes les cerques i visites que fem i les poden associar fàcilment amb la nostra identitat, ja sigui de manera directa, per exemple perquè hi tenim un copte obert, o bé indirectament creuant dades (IP, tipus de consultes, etc.) 
        * _Recomanació:_ https://duckduckgo.com/
* Registres
    * Nostres (que poden eliminar):
        * Històric de llistes de trucades
        * Punts d'accés a on ens hem associat
        * Xats descontrolats
        * etc.
    * De tercers (que hem d'evitar)
        * Hores de pas per un radar de l'autopista
        * Hores de commits :p
        * etc.

## Diccionari i acrònims
_**ISP**_ Proveïdor d'accés a internet (de l'anglès Internet Service Provider). De manera general, qualsevol empresa que ofereixi serveis a internet. De vegades es restringeix als proveïdors de xarxa, també anomenades operadores (Telefonica, Vodafone, etc.), excloent-ne els proveïdor de continguts (Netflix, Google, etc.).
